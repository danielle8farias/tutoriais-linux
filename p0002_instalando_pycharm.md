# Instalando Pycharm (Ubuntu, Mint e derivados do Debian)

_**Atenção!**_

É necessário ter o Python já instalado na sua máquina antes de prosseguir. Caso ainda não o tenha, você pode usar [esse tutorial](p0000_instalando_pyenv.md).

----

Antes de baixar o pacote para a instalação do Pycharm, vou criar um diretório para ele na minha *home*. Você pode pular essa parte caso ache desnecessário ou escolher uma pasta diferente para fazer a sua instalação.

Sendo assim, no terminal digite:

```
$ mkdir .opt
```

- **$** indica que você deve usar o usuário comum para fazer essa operação.

- **mkdir** do inglês, *make directory*, é o comando que vai criar o nosso novo diretório/pasta.

- **.opt** é o nome que eu escolhi para a minha pasta. Ela leva um ponto na frente porque quero que ela fique oculta.

Agora, vamos entrar na pasta

```
$ cd .opt/
```

- **cd** do inglês, *change directory*, serve para mudar de diretório/pasta.


## Baixando o pacote do instalador

Vamos baixar a versão *Community* do programa.

```
$ wget https://download.jetbrains.com/python/pycharm-community-2023.2.1.tar.gz
```

- O comando **wget** serve para fazer downloads de arquivos.

> A sua versão do programa pode ser diferente desse tutorial. Lembre-se de verificar a última versão disponível no [site oficial do Pycharm](https://www.jetbrains.com/pt-br/pycharm/download).

![baixando o pacote do instalador](img/p0002-0.png)

![ls da pasta .opt](img/p0002-1.png)

- **ls** é o comando usado para listar o conteúdo de um diretório.


## Extraindo o arquivo

Para prosseguir com a instalação, precisamos extrair o arquivo.

```
$ tar -xvzf pycharm-community-2021.2.3.tar.gz
```

- **tar** é um software que permite unir dois ou mais arquivos em um. Ele é usado como um complemento para o compactador, mas não é o compactador.
- O parâmetro **x** é o que extrai os arquivos.
- O parâmetro **v** exibe os detalhes dessa operação.
- O parâmetro **z** indica o tipo da extensão da compactação, que no caso é o **.gz**.
- O parâmetro **f** especifica o arquivo que será usado; seguido do nome do arquivo e sua extensão.
- A extensão **gz** é o nosso compactador.

Se o deu tudo certo, foi criada uma pasta nova.

![pasta com o instalador do pycharm](img/p0002-2.png)


## Instalando o Pycharm

Dentro dessa pasta nova que foi criada, vamos procurar pela pasta **bin** e entrar nela.

```
$ cd pycharm-community-2021.2.3/bin
```

E então executamos o instalador

```
$ ./pycharm.sh
```

- O **ponto-barra** juntos serve para indicar o caminho do executável;
- **ponto** significa diretório atual;
- **barra** serve para separar o diretório do nome do arquivo.

![instalando o pycharm](img/p0002-3.png)

Basta aceitar os termos da comunidade que a instalação se dará automaticamente.


## Criando um lançador para o Pycharm

Para criar um **lançador** para o sistema, na tela inicial do Pycharm, basta ir na **engrenagem** e clicar em **Create Desktop Entry**. Como mostrado na imagem abaixo.

![criando o lançador](img/p0002-4.png)

A última coisa a ser feita é navegar de volta a pasta .opt e remover o pacote de instalação que não será mais usado.

```
$ rm pycharm-community-2023.2.1.tar.gz
```

Agora você já pode criar seus projetos usando o Pycharm.


tags: python, pycharm, tutorial, linux
