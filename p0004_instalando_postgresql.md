# Instalando o PostgreSQL 

_**Atenção!**_

Esse tutorial foi feito usando o S.O. Linux Mint 20.3 Una; versão 64 bits. Se você estiver usando uma versão ou distribuição diferente, alguns comandos também podem ser diferentes.

----

Primeiramente é preciso instalar as dependências (caso você ainda não as tenha):

Abra um terminal e digite o comando abaixo:

```
$ sudo apt install git vim build-essential software-properties-common gnupg apt-transport-https ca-certificates libssl-dev libffi-dev libgmp3-dev virtualenv python3-pip libpq-dev python-dev libexpat1 ssl-cert
```

- **$** é mostrado no seu terminal e indica que você deve usar o usuário comum para fazer essa operação, não é necessário escrever isso no terminal novamente, você deve apenas confirmar se isso aparece.

- **sudo** serve para pedir permissões de administrador temporariamente (pode ser necessário que você digite sua senha).

- **apt** do inglês, *Advanced Package Tool*, em português, Ferramenta de Empacotamento Avançada; é a ferramenta que nos ajuda na instalação, atualização e desinstalação de programas, entre outras funções.

- **install** é o comando de instalar, indicando ao apt o que fazer.

- **git** é um sistema de controle de versão.

- **build-essential** são meta-pacotes necessários para a compilação de programas.

- **software-properties-common** fornece alguns scripts úteis para a adição e remoção de PPAs (Personal Package Archives)

- **gnupg** é uma aplicação que permite criptografar assinaturas e dados. 

- **apt-transport-https** permite o acesso de repositórios por meios mais seguros.

- **ca-certificates** entidade que armazena assinaturas e certificados digitais.

- **libssl-dev** biblioteca de implementação de protocolos de segurança.

- **libffi-dev** biblioteca de interface de função externa.

- **libgmp3-dev** biblioteca usada para aumentar a precisão aritmética de cálculos e números.

- **virtualenv** ferramenta usada para criar ambientes isolados, impedindo conflitos com o sistema operacional.

- **python3-pip** gerenciador de pacotes de programas escritos em Python.

- **libpq-dev** biblioteca para conexão TCP/IP do PostgreSQL.

- **python-dev** pacote de desenvolvimento que contém cabeçalhos requeridos para criação de extensões em Python.

- **libexpat1** biblioteca e cabeçalhos de desenvolvimento do expat.

- **ssl-cert** certificado SSL, que torna possível a criptografia SSL/TLS.

Agora precisamos baixar e instalar a Chave GPG do PostgreSQL:

```
$ curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/apt.postgresql.org.gpg > /dev/null
```

- **curl** é uma ferramenta em linha de comando para transferência de dados com sintaxe URL.

- **f** parâmetro para interromper o download em caso de erro HTTP.

- **s** parâmetro para que a saída da requisição seja silenciosa, sem mostrar na tela as etapas desta.

- **S** parâmetro para mostrar o erro HTTP na saída do terminal, se houver.

- **L** parâmetro para seguir redirecionamentos HTTP.

- **https://www.postgresql.org/media/keys/ACCC4CF8.asc** é a URL do arquivo contendo a chave de assinatura GPG do repositório do PostgreSQL.

- **|** , esse símbolo é conhecido como pipe. Ele pega a saída de um comando dado à esquerda e o coloca como entrada para o comando da direita.

- **gpg** é usado para criptografia e assinatura digital de arquivos e mensagens.

- **dearmor** parâmetro usado para descriptografar a chave baixada em formato ASCII-armored e convertê-la em um formato binário.

- **tee** é usado para redirecionar a saída de um comando para um arquivo ou para outro comando.

- **/etc/apt/trusted.gpg.d/apt.postgresql.org.gpg** indica o arquivo onde a chave de assinatura GPG será instalada. 

- **> /dev/null** é usada para redirecionar a saída do comando para o dispositivo "nulo", que descarta a saída e não a exibe na tela.


Adicionando o repositório do PostgreSQL ao APT:

```
$ sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt focal-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
```

- **sh -c** é para chamar um novo shell, garantindo que o comando seja executado em um ambiente limpo e isolado.

- **'echo "deb http://apt.postgresql.org/pub/repos/apt focal-pgdg main" > /etc/apt/sources.list.d/pgdg.list'** é o comando que será executado pelo novo shell.

- **echo** escreve na tela; nesse caso vai escrever o que o shell precisa executar.

- entre **" "** vem o que o comando echo deve escrever.

- **focal** é o codinome do sistema operacional o qual o Mint é baseado, que no meu caso é o Ubuntu.

- **>** pega a saída do comando echo e coloca em um arquivo.

- **/etc/apt/sources.list.d/pgdg.list** é o endereço, juntamente com o arquivo que será criado; nesse caso o arquivo "pgdg.list".

Agora atualizamos a lista do APT com o novo repositório do PostgreSQL que adicionamos acima:

```
$ sudo apt update
```

- **update** é o comando para atualizar a lista dos repositórios.


Agora vamos instalar o PostgreSQL Server e Client, com o comando:

```
$ sudo apt install postgresql postgresql-contrib postgresql-client
```

Feita a instalação com sucesso, precisamos recarregar a configuração do **systemd**, que é um sistema de gerenciamento de serviços presente em muitas distribuições Linux.

```
$ sudo systemctl daemon-reload
```

- **systemd** é responsável por iniciar e gerenciar os serviços em segundo plano no sistema operacional.
- **daemon-reload** é para recarregar a configuração do **systemd**. Isso é necessário porque quando você faz alterações em um arquivo de serviço, adicionando ou removendo uma unidade de serviço, o **systemd** não as reconhece imediatamente.

Agora vamos habilitar o serviço do PostgreSQL:

```
$ sudo systemctl enable postgresql
```

- **enable**, do inglês, habilitar, seguido do nome do serviço desejado, no caso o postgresql.

Após habilitar, basta **iniciar** o serviço com o comando:

```
$ sudo systemctl start postgresql
```

Agora precisamos verificar o status do serviço através do comando:

```
$ sudo systemctl status postgresql
```

Você verá um retorno parecido com a imagem abaixo na tela.

![retorno de status do postgresql](img/p0004-0.png)

Alguns comando importantes são para **reiniciar**

```
$ sudo systemctl restart postgresql
```

Toda vez que você mudar as configurações do servidor, você precisará fazer recarregá-lo.

Para **parar** o serviço, use:

```
$ sudo systemctl stop postgresql
``` 

Agora, vamos verificar a porta padrão de conexão do PostgreSQL (5432):

```
$ sudo lsof -nP -iTCP:'5432' -sTCP:LISTEN
```

O retorno deverá ser algo como a imagem abaixo:

![verificando a porta do postgresql](img/p0004-1.png)

Você pode verificar que no endereço

```
/etc/postgresql/15/main/
```

ficam dois arquivos de configurações importantes do PostgreSQL.

![arquivos de configurações do postgresql](img/p0004-2.png)

O **postgresql.conf** responsável pela configuração em si, do servidor e o **pg_hba.conf** que é um arquivo importante para acesso remoto, balanceamento de carga e de liberação da rede.

E no endereço

```
/var/log/postgresql/
```

ficam salvos os logs e no endereço

```
/var/lib/postgresql/15/main
```

onde são salvos, por padrão, os bancos de dados.

Agora vamos adicionar o seu usuário linux no grupo do PostgreSQL para permitir a administração sem problemas futuros.

Primeiro vamos digitar, no terminal, o comando:

```
$ id
```

- **id** este comando informa o UID, o GID e os grupos de um determinado usuário.

![id do usuário](img/p0004-3.png)

Como é possível verificar pela imagem do retorno no terminal, meu usuário ainda pertence ao grupo do PostgreSQL.

Agora vamos, efetivamente, adicionar nosso usuário ao grupo do PostgreSQL:

```
$ sudo usermod -a -G postgres $USER
```

- **usermod** comando utilizado para modificar usuários.

- **a** parâmetro utilizado para adicionar o usuário a um novo grupo.

- **G** parâmetro que indica o nome do grupo que estamos adicionando o usuário. Nesse caso, estamos adicionando o usuário ao grupo **"postgres"**.

- **postgres** nome do grupo que será adicionado o usuário.

- **$USER** é uma variável de ambiente que contém o nome do usuário atualmente conectado ao sistema.

Reinicie a máquina para que o seu usuário apareça adicionado ao novo grupo.

```
$ sudo reboot
```

----

## Verificando a versão

É possível verificar a versão do seu PostgreSQL instalado de dois modos. Através do comando:

```
$ /usr/lib/postgresql/15/bin/postgres --version
```

ou do comando:

```
$ psql --version
```

----

## Alguns comando do cliente da linha de comando

Para iniciar o cliente da linha de comando, digite:

```
$ sudo -u postgres psql
```

- **u** é parâmetro do comando sudo que permite que você especifique o usuário que deve executar o comando seguinte. Nesse caso do comando sudo, estamos especificando que o usuário **postgres** que deve executar o comando **psql**.

- **postgres** é o nome do usuário que está sendo especificado pelo parâmetro **-u**.

- **psql** é o comando que inicia o cliente de linha de comando.

Uma vez que o cliente de linha de comando do PostgreSQL seja iniciado, você pode executar comandos SQL para gerenciar o banco de dados.

![cliente de linha de comando do PostgreSQL](img/p0004-4.png)

Tais como:

Para listar todos os bancos de dados

```
\l
```

![listando banco de dados](img/p0004-5.png)

Para se conectar a um banco de dados existente, digite

```
\c <nome do bando de dados>
```

- digite o nome do banco sem os sinais **< e >**. Exemplo:

```
\c postgres
```

![conectando com um banco de dados](img/p0004-6.png)

Para listar as tabelas do banco ao qual você está conectado, digite:

```
\dt
```

Para listar a tablespace (lugar no sistema de arquivos onde serão armazenados os objetos)

```
\db
```

Para verificar informações sobre grupos de usuários e seus membros

```
\dg
```

Para mostrar informações sobre usuários e suas permissões de acesso, digite:

```
\du
```

Para ver as informações do banco de dados

```
\conninfo
```

Para sair do cliente de linha de comando, digite:

```
\q
```

Caso queira configurar a senha do usuário administrador do PostgreSQL, use o comando:

```
$ sudo -u postgres psql --command '\password postgres'
```

- **command** é o parâmetro do comando **psql** que especifica um comando a ser executado.
- **'\password postgres'** é usado para definir a senha do usuário "postgres" para conexão remota.

----

## Permitindo o acesso remoto do PostgreSQL




tags: postgresql, install, sql
