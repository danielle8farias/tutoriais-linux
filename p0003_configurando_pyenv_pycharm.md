# Configurando o Pyenv no Pycharm

_**Atenção!**_

É necessário ter alguma versão do Python já instalada na sua máquina antes de prosseguir. Caso ainda não o tenha, você pode usar [esse tutorial](p0000_instalando_pyenv.md).

Caso precise saber como instalar o Pycharm, [clique aqui](p0002_instalando_pycharm.md).

----

Com o Pycharm aberto, vá em **file**. Em seguida **settings**.

![file, settings](img/p0003-0.png)

Em **Project: python Project** e em **Python interpreter**.

![python project](img/p0003-1.png)

Em **Python Interpreter**, onde está escrito **<No interpreter\>**, clique em **Show All...**

![no interpreter](img/p0003-2.png)

Em seguida clique no sinal de mais.

![sinal de mais](img/p0003-3.png)

Em **Virtualenv Environment**, marque a opção **Existing environment** e na parte do Interpreter, clique nos **três pontos**.

![existing environment](img/p0003-4.png)

Abrirá uma janela **Select Python Interpreter**. Aqui você vai selecionar o caminho até a versão do Python do Pyenv que deseja usar.

![caminho até o interpretador python](img/p0003-5.png)

Na maioria das vezes esse caminho é parecido com a mostrada na imagem, com algumas alterações.

```
/home/seu usuário/.pyenv/versions/número da versão que você instalou/bin/pythonX.XX
```

O que muda possivelmente é o **nome da sua máquina (seu usuário)**, o **número da versão do Python que você instalou** com o Pyenv e o nome do interpretador **pythonX.XX**, onde os Xs referem-se ao número da sua versão.

Após, marque a opção **Make available to all projects** e clique em **OK**.

![Make available to all projects](img/p0003-6.png)

Agora basta confirmar a tela seguinte e clicar em **Apply** na janela **Settings**.

![conferindo a versão do interpretador](img/p0003-7.png)

![aplicando as configurações](img/p0003-8.png)


----

Abaixo um gif mostrando esse passo a passo.

![configurando o pyenv no pycharm](img/p0003-9.gif)


tags: python, pyenv, pycharm, tutorial
