# Instalando o Python com o Pyenv

## O que é o Pyenv?

O Pyenv é um gerenciador de ambientes Python. 
De maneira resumida, é uma ferramente que te permite escolher entre diversas versões do Python para usar.

## Instalando dependências

Antes de começar a instalação é necessário verificar se o sistema possui algumas bibliotecas que nos ajudarão a compilar e instalar o Pyenv.
No terminal, digite o comando:

```
$ sudo apt install build-essential checkinstall
```

- **$** indica que você deve usar o usuário comum para fazer essa operação.
- **sudo** serve para pedir permissões de administrador temporariamente.
- **apt** do inglês, Advanced Package Tool, em português, Ferramenta de Empacotamento Avançada; é a ferramenta que nos ajuda na instalação, atualização e desinstalação de programas, entre outras funções.
- **install** é o comando de instalar, indicando ao apt o que fazer.
- **build-essential** é uma biblioteca que reúne diversas aplicações para compilar e instalar outros programas, que inclui, por exemplo, o make, automake, etc.

Vamos instalar também outras bibliotecas de desenvolvimento que nos ajudarão na compilação do código fonte:

```
$ sudo apt install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev
```

## Instalando o Pyenv

De acordo com o [repositório oficial da ferramente no GitHub](https://github.com/pyenv/pyenv-installer), para instalar o Pyenv, precisamos digitar no terminal o seguinte comando:

```
$ curl https://pyenv.run | bash
```

- **curl** é uma ferramenta em linha de comando para transferência de dados com sintaxe URL.
- **|** , esse símbolo é conhecido como pipe. Ele pega a saída de um comando dado à esquerda e o coloca como entrada para o comando da direita.
- **bash** é o shell.

> [Não sabe o que é o Shell? Clica aqui que eu te conto!](https://dev.to/womakerscode/o-que-e-o-shell-3cjf)

![instalação do pyenv](img/p0000-0.png)

Agora vamos abrir o arquivo .bashrc 

```
$ vim .bashrc
```

ou 

```
$ vim ~/.bashrc
```

Caso você não esteja na sua *home*.

Aqui, estou usando o **VIM**, mas você pode usar qualquer editor de sua preferência.

> [Para saber mais sobre o VIM, clique aqui.](https://dev.to/womakerscode/dicas-do-vim-instalando-o-vim-neovim-no-linux-4k6b)

e acrescentar ao final do arquivo essas linhas:

```
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"
```

Você pode usar o editor de texto que preferir para fazer isso.

> atenção para não esquecer de salvar o arquivo antes de sair do editor.

Agora basta reiniciar o Shell, fechando a janela do Terminal e abrindo outra, ou digitar o comando

```
$ exec $SHELL
```

Ao digitar 

```
$ pyenv
```

no terminal e a instalação tiver sida feito com sucesso, o retorno será algo parecido com

![pyenv funcionando](img/p0000-1.png)


## Instalando a sua versão preferida do Python

A partir de agora podemos escolher qual versão do Python instalar com a ajuda do Pyenv. Para isso basta digitar,

```
$ pyenv install -list
```

![versões do python para instalar](img/p0000-2.png)

Vou escolher a última versão estável que tenho disponível (no momento em que escrevo esse tutorial):

![escolhendo a versão 3.10.0 do python](img/p0000-3.png)

Sendo assim digite

```
$ pyenv install <número da versão do Python que deseja instalar>
```

- digite o número da versão do Python que deseja instalar sem os sinais **< e >**.

No exemplo, estou instalando a versão 3.10.0

![instalando a versão 3.10.0 do python](img/p0000-4.png)

Com o comando 

```
$ pyenv versions
```

é possível conferir as versões do Python instaladas com o auxílio do Pyenv.

![verificando todas as versões instaladas com o pyenv e qual está selecionada](img/p0000-5.png)

Como podemos ver na imagem, embora eu já tenha instalado a última versão, a versão que está selecionada para uso é a do sistema. Sendo assim podemos fazer a mudança com o comando

```
$ pyenv global <número da versão do Python que deseja usar>
```

No exemplo, usei

```
$ pyenv global 3.10.0
```

Agora, ao conferir de novo o comando **pyenv versions**, veremos a outra versão selecionada

![selecionada a versão 3.10.0](img/p0000-6.png)

Reiniciamos novamente o Shell

```
$ exec $SHELL
```

Ao digitar no terminal o comando

```
$ which python
```

O retorno será

![mostrando qual versão o sistema está usando](img/p0000-7.png)

indicando que a versão do Pyenv é a que está sendo utilizada.

Você também pode digitar

```
$ python -V
```

para conferir a versão que está sendo usada.

![mostrando qual versão o sistema está usando](img/p0000-8.png)


tags: pyenv, python, tutorial, install
